To run application in Intelij IDEA lombok plugin should be installed.
https://projectlombok.org/setup/intellij

## What needs to be installed
1. Java 8 or higher.
2. Postgres 9.6. On database server database behavox need to be created. DB url, username and password could be ovverided in application property
3. Optionally docker need to be installed in case to run app as docker container

## General info
Behavox-api-server is test application. For building application maven is used.
After application has been started web server (JETTY) will listen HTTP request on port 8600
Spring Boot 2 is used as main framework for application
```

## How to run tests
To run tests please execute command mvn test (it will execute several integration test based on MOCKMVC)

## API docs
To view API docs please do next:
- run app voa any IDE or via maven - mvn spring-boot:run
- acces via browser http://localhost:8600/swagger-ui.html

## Run in docker
To run application in docker next step should be done:
- run mvn clean install to generate target jar for image
- run docker-compose up. It will download postgres image and will build application image from jar