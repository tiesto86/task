FROM java:8-jdk-alpine

COPY ./target/behavox-api-server-1.0.0.jar /usr/app/

WORKDIR /usr/app

RUN sh -c 'touch behavox-api-server-1.0.0.jar'

ENTRYPOINT ["java","-jar","behavox-api-server-1.0.0.jar"]