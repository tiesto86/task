package integration;

import com.behavox.dto.auth.JwtAuthenticationResponse;
import com.behavox.dto.auth.LoginDTO;
import com.behavox.entity.audit.TaskAudit;
import com.behavox.repository.audit.RevinfoRepository;
import com.behavox.repository.audit.TaskAuditRepository;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.behavox.BehavoxApplication;
import com.opentable.db.postgres.embedded.FlywayPreparer;
import com.opentable.db.postgres.junit.EmbeddedPostgresRules;
import com.opentable.db.postgres.junit.PreparedDbRule;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@SpringBootTest(classes = {BehavoxApplication.class, TestConfiguration.class})
@Ignore
@Slf4j
public class TestsSetup {

	protected MockMvc mvc;

	protected static ObjectMapper objectMapper = new ObjectMapper();

	@Autowired
	protected WebApplicationContext webApplicationContext;

	@Autowired
	private FilterChainProxy springSecurityFilterChain;

	@Autowired
	private RevinfoRepository revinfoRepository;

	@Autowired
	private TaskAuditRepository taskAuditRepository;

	@Rule
	public PreparedDbRule db =
			EmbeddedPostgresRules.preparedDatabase(
					FlywayPreparer.forClasspathLocation("db/migration"));


	@Before
	public void setup() throws IOException {
		mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
				.addFilters(springSecurityFilterChain)
				.build();
		revinfoRepository.deleteAll();
		taskAuditRepository.deleteAll();
	}
	
	/*
	 * Static methods
	 */

	public static <T> T getObjectFromResponse(MvcResult result, Class<T> returnType)
			throws IOException {
		String json = result.getResponse().getContentAsString();
		return objectMapper.readValue(json, returnType);
	}

	public static <T> List<T> getListFromResponse(MvcResult result, Class<T> type) throws IOException {
		CollectionType listType = objectMapper.getTypeFactory().constructCollectionType(List.class, type);
		return objectMapper.readValue(result.getResponse().getContentAsString(), listType);
	}
}
