package integration;

import com.behavox.dto.ErrorDetails;
import com.behavox.dto.auth.JwtAuthenticationResponse;
import com.behavox.dto.auth.LoginDTO;
import com.behavox.dto.tasks.PatchDTO;
import com.behavox.dto.tasks.TaskDTO;
import com.behavox.dto.tasks.TaskFilterDTO;
import com.behavox.dto.tasks.TaskResponseDTO;
import com.behavox.entity.Status;
import com.behavox.entity.Task;
import com.behavox.entity.User;
import com.behavox.repository.StatusRepository;
import com.behavox.repository.TaskRepository;
import com.behavox.repository.UserRepository;
import com.behavox.service.StatusService;
import com.google.common.collect.Lists;
import integration.helpers.LoginControllerTestHelper;
import integration.helpers.TasksControllerTestHelper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import java.util.List;

public class TasksControllerTest extends TestsSetup {

    private TasksControllerTestHelper helper;

    private LoginControllerTestHelper authhelper;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private StatusRepository statusRepository;

    private JwtAuthenticationResponse adminToken;

    private JwtAuthenticationResponse userToken;

    @Before
    public void before() throws Exception {
        super.setup();
        this.helper = TasksControllerTestHelper.start(mvc);
        this.authhelper = LoginControllerTestHelper.start(mvc);
        taskRepository.deleteAll();

        LoginDTO admin = LoginDTO.builder()
                .password("admin")
                .username("admin@test.com")
                .build();
        adminToken = authhelper.doLogin(admin, HttpStatus.OK, JwtAuthenticationResponse.class);
        LoginDTO user = LoginDTO.builder()
                .password("user")
                .username("user@test.com")
                .build();
        userToken = authhelper.doLogin(user, HttpStatus.OK, JwtAuthenticationResponse.class);
    }

    @Test
    public void createTaskUnderAdmin() throws Exception{
        TaskDTO taskDTO = TasksControllerTestHelper.getTaskToCreate();
        TaskResponseDTO created = helper.createTask(taskDTO, LoginControllerTestHelper.getAccessToken(adminToken), HttpStatus.CREATED, TaskResponseDTO.class);
        Assert.assertEquals(taskDTO.getDescription(), created.getDescription());
        Assert.assertEquals(taskDTO.getName(), created.getName());
        Assert.assertNotNull(created.getId());
        Assert.assertNull(created.getAssigned());

        User adminuser= ((List<User>)userRepository.findAll()).stream()
                .filter(user -> user.getEmail().equals("admin@test.com"))
                .findFirst().get();
        Status pendingStatus= ((List<Status>)statusRepository.findAll()).stream()
                .filter(status -> status.getName().equals(StatusService.OPENED))
                .findFirst().get();
        List<Task> tasks = ((List<Task>)taskRepository.findAll());
        Assert.assertEquals(1, tasks.size());
        Assert.assertTrue(tasks.get(0).getActive());
        Assert.assertEquals(adminuser.getId(), tasks.get(0).getCreatedBy());
        Assert.assertNull(tasks.get(0).getAssignedUser());
        Assert.assertEquals(adminuser.getId(), tasks.get(0).getUpdatedBy());
        Assert.assertEquals(pendingStatus.getId(), tasks.get(0).getStatus().getId());
        Assert.assertNotNull(tasks.get(0).getCreatedDate());
        Assert.assertNotNull(tasks.get(0).getUpdatedDate());
        Assert.assertEquals(tasks.get(0).getUpdatedDate(), tasks.get(0).getCreatedDate());
    }

    @Test
    public void updateTaskUnderAdmin() throws Exception{
        TaskDTO taskDTO = TasksControllerTestHelper.getTaskToCreate();
        TaskResponseDTO created = helper.createTask(taskDTO, LoginControllerTestHelper.getAccessToken(adminToken), HttpStatus.CREATED, TaskResponseDTO.class);
        TaskDTO updateTaskDTO = TasksControllerTestHelper.getTaskToUpdate(created.getId(), "dd", "cc");
        TaskResponseDTO updated = helper.updateTask(updateTaskDTO, LoginControllerTestHelper.getAccessToken(adminToken), HttpStatus.OK, TaskResponseDTO.class);

        Assert.assertEquals("cc", updated.getDescription());
        Assert.assertEquals("dd", updated.getName());
        Assert.assertNull(created.getAssigned());

        User adminuser= ((List<User>)userRepository.findAll()).stream()
                .filter(user -> user.getEmail().equals("admin@test.com"))
                .findFirst().get();
        Status pendingStatus= ((List<Status>)statusRepository.findAll()).stream()
                .filter(status -> status.getName().equals(StatusService.OPENED))
                .findFirst().get();
        List<Task> tasks = ((List<Task>)taskRepository.findAll());
        Assert.assertEquals(1, tasks.size());
        Assert.assertTrue(tasks.get(0).getActive());
        Assert.assertEquals(adminuser.getId(), tasks.get(0).getCreatedBy());
        Assert.assertNull(tasks.get(0).getAssignedUser());
        Assert.assertEquals(adminuser.getId(), tasks.get(0).getUpdatedBy());
        Assert.assertEquals(pendingStatus.getId(), tasks.get(0).getStatus().getId());
        Assert.assertNotNull(tasks.get(0).getCreatedDate());
        Assert.assertNotNull(tasks.get(0).getUpdatedDate());
        Assert.assertEquals("cc", tasks.get(0).getDescription());
        Assert.assertEquals("dd", tasks.get(0).getName());
        Assert.assertNotEquals(tasks.get(0).getUpdatedDate(), tasks.get(0).getCreatedDate());
    }

    @Test
    public void createTaskUnderUser() throws Exception{
        TaskDTO taskDTO = TasksControllerTestHelper.getTaskToCreate();
        ErrorDetails error = helper.createTask(taskDTO, LoginControllerTestHelper.getAccessToken(userToken), HttpStatus.FORBIDDEN, ErrorDetails.class);
        Assert.assertEquals("Access is denied", error.getMessage());
    }

    @Test
    public void updateTaskUnderUser() throws Exception{
        TaskDTO taskDTO = TasksControllerTestHelper.getTaskToCreate();
        TaskResponseDTO created = helper.createTask(taskDTO, LoginControllerTestHelper.getAccessToken(adminToken), HttpStatus.CREATED, TaskResponseDTO.class);
        created.setDescription("ccc");
        ErrorDetails error = helper.updateTask(taskDTO, LoginControllerTestHelper.getAccessToken(userToken), HttpStatus.FORBIDDEN, ErrorDetails.class);
        Assert.assertEquals("Access is denied", error.getMessage());
    }

    @Test
    public void getTaskById() throws Exception{
        TaskDTO taskDTO = TasksControllerTestHelper.getTaskToCreate();
        TaskResponseDTO created = helper.createTask(taskDTO, LoginControllerTestHelper.getAccessToken(adminToken), HttpStatus.CREATED, TaskResponseDTO.class);
        TaskResponseDTO got = helper.getTaskById(created.getId(), LoginControllerTestHelper.getAccessToken(userToken), HttpStatus.OK, TaskResponseDTO.class);
        Assert.assertNotNull(got);
        Assert.assertEquals(created.getDescription(), got.getDescription());
        Assert.assertEquals(created.getName(), got.getName());
        Assert.assertEquals(created.getId(), got.getId());
        Assert.assertEquals(created.getStatus(), got.getStatus());
    }

    @Test
    public void getTaskByUnexistingId() throws Exception{
        ErrorDetails errorDetails = helper.getTaskById(999999L, LoginControllerTestHelper.getAccessToken(userToken), HttpStatus.NOT_FOUND, ErrorDetails.class);
        Assert.assertEquals("Task not found by id: 999999", errorDetails.getMessage());
    }

    @Test
    public void deleteTask() throws Exception{
        TaskDTO taskDTO = TasksControllerTestHelper.getTaskToCreate();
        TaskResponseDTO created = helper.createTask(taskDTO, LoginControllerTestHelper.getAccessToken(adminToken), HttpStatus.CREATED, TaskResponseDTO.class);
        helper.deleteTaskById(created.getId(), LoginControllerTestHelper.getAccessToken(adminToken), HttpStatus.OK);

        //try to delete it again
        ErrorDetails errorDetails =  helper.deleteTaskById(created.getId(), LoginControllerTestHelper.getAccessToken(adminToken), HttpStatus.NOT_FOUND, ErrorDetails.class);
        Assert.assertEquals("Task not found by id: " + created.getId(), errorDetails.getMessage());

        //try to get it
        errorDetails =  helper.getTaskById(created.getId(), LoginControllerTestHelper.getAccessToken(adminToken), HttpStatus.NOT_FOUND, ErrorDetails.class);
        Assert.assertEquals("Task not found by id: " + created.getId(), errorDetails.getMessage());
    }

    @Test
    public void getAllMyTasks() throws Exception{
        TaskDTO taskDTO = TasksControllerTestHelper.getTaskToCreate();
        TaskResponseDTO created = helper.createTask(taskDTO, LoginControllerTestHelper.getAccessToken(adminToken), HttpStatus.CREATED, TaskResponseDTO.class);

        //I don't have any tasks
        TaskFilterDTO taskFilterDTO = TaskFilterDTO.builder().build();
        List<TaskResponseDTO> tasks =  helper.getMyTasks(taskFilterDTO, LoginControllerTestHelper.getAccessToken(userToken), HttpStatus.OK, TaskResponseDTO.class);
        Assert.assertEquals(0, tasks.size());

        //try to get it
        PatchDTO patchDTO = PatchDTO.builder()
                .assigned(2L)
                .build();
        helper.patchTask(created.getId(), patchDTO, LoginControllerTestHelper.getAccessToken(userToken), HttpStatus.OK, TaskResponseDTO.class);
        tasks =  helper.getMyTasks(taskFilterDTO, LoginControllerTestHelper.getAccessToken(userToken), HttpStatus.OK, TaskResponseDTO.class);
        Assert.assertEquals(1, tasks.size());

        //I have one opened
        taskFilterDTO = TaskFilterDTO.builder().statuses(Lists.newArrayList(1L)).build();
        tasks =  helper.getMyTasks(taskFilterDTO, LoginControllerTestHelper.getAccessToken(userToken), HttpStatus.OK, TaskResponseDTO.class);
        Assert.assertEquals(1, tasks.size());

        //I don't have anything in progress
        taskFilterDTO = TaskFilterDTO.builder().statuses(Lists.newArrayList(2L)).build();
        tasks =  helper.getMyTasks(taskFilterDTO, LoginControllerTestHelper.getAccessToken(userToken), HttpStatus.OK, TaskResponseDTO.class);
        Assert.assertEquals(0, tasks.size());
    }
}
