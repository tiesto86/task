package integration;

import com.behavox.dto.ErrorDetails;
import com.behavox.dto.auth.JwtAuthenticationResponse;
import com.behavox.dto.auth.LoginDTO;
import com.behavox.dto.tasks.PatchDTO;
import com.behavox.dto.tasks.TaskDTO;
import com.behavox.dto.tasks.TaskResponseDTO;
import com.behavox.repository.TaskRepository;
import integration.helpers.LoginControllerTestHelper;
import integration.helpers.TasksControllerTestHelper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

public class TasksTransitionsControllerTest extends TestsSetup {

    private TasksControllerTestHelper helper;

    private LoginControllerTestHelper authhelper;

    @Autowired
    private TaskRepository taskRepository;

    private JwtAuthenticationResponse adminToken;

    private JwtAuthenticationResponse userToken;

    @Before
    public void before() throws Exception {
        super.setup();
        this.helper = TasksControllerTestHelper.start(mvc);
        this.authhelper = LoginControllerTestHelper.start(mvc);
        taskRepository.deleteAll();

        LoginDTO admin = LoginDTO.builder()
                .password("admin")
                .username("admin@test.com")
                .build();
        adminToken = authhelper.doLogin(admin, HttpStatus.OK, JwtAuthenticationResponse.class);
        LoginDTO user = LoginDTO.builder()
                .password("user")
                .username("user@test.com")
                .build();
        userToken = authhelper.doLogin(user, HttpStatus.OK, JwtAuthenticationResponse.class);
    }

    @Test
    public void fromOpenToProgress() throws Exception{
        TaskDTO taskDTO = TasksControllerTestHelper.getTaskToCreate();
        TaskResponseDTO created = helper.createTask(taskDTO, LoginControllerTestHelper.getAccessToken(adminToken), HttpStatus.CREATED, TaskResponseDTO.class);
        PatchDTO patchDTO = PatchDTO.builder()
                .status(2L)
                .build();
        TaskResponseDTO updated = helper.patchTask(created.getId(), patchDTO, LoginControllerTestHelper.getAccessToken(userToken), HttpStatus.OK, TaskResponseDTO.class);
        Assert.assertEquals("IN PROGESS", updated.getStatus());

    }

    @Test
    public void fromOpenToUnexistingStatus() throws Exception{
        TaskDTO taskDTO = TasksControllerTestHelper.getTaskToCreate();
        TaskResponseDTO created = helper.createTask(taskDTO, LoginControllerTestHelper.getAccessToken(adminToken), HttpStatus.CREATED, TaskResponseDTO.class);
        PatchDTO patchDTO = PatchDTO.builder()
                .status(222L)
                .build();
        ErrorDetails errorDetails = helper.patchTask(created.getId(), patchDTO, LoginControllerTestHelper.getAccessToken(userToken), HttpStatus.NOT_FOUND, ErrorDetails.class);
        Assert.assertEquals("Not found status by ID: 222", errorDetails.getMessage());

    }

    @Test
    public void fromOpenToUnsopportedStatus() throws Exception{
        TaskDTO taskDTO = TasksControllerTestHelper.getTaskToCreate();
        TaskResponseDTO created = helper.createTask(taskDTO, LoginControllerTestHelper.getAccessToken(adminToken), HttpStatus.CREATED, TaskResponseDTO.class);
        PatchDTO patchDTO = PatchDTO.builder()
                .status(4L)
                .build();
        ErrorDetails errorDetails = helper.patchTask(created.getId(), patchDTO, LoginControllerTestHelper.getAccessToken(userToken), HttpStatus.BAD_REQUEST, ErrorDetails.class);
        Assert.assertEquals("This action is not allowed! Next possible action: IN PROGESS,RESOLVED,CLOSED", errorDetails.getMessage());

    }

    @Test
    public void assignToUser() throws Exception{
        TaskDTO taskDTO = TasksControllerTestHelper.getTaskToCreate();
        TaskResponseDTO created = helper.createTask(taskDTO, LoginControllerTestHelper.getAccessToken(adminToken), HttpStatus.CREATED, TaskResponseDTO.class);
        PatchDTO patchDTO = PatchDTO.builder()
                .assigned(1L)
                .build();
        TaskResponseDTO updated = helper.patchTask(created.getId(), patchDTO, LoginControllerTestHelper.getAccessToken(userToken), HttpStatus.OK, TaskResponseDTO.class);
        Assert.assertEquals(1, updated.getAssigned().intValue());

    }

    @Test
    public void assignToUnexistingUser() throws Exception{
        TaskDTO taskDTO = TasksControllerTestHelper.getTaskToCreate();
        TaskResponseDTO created = helper.createTask(taskDTO, LoginControllerTestHelper.getAccessToken(adminToken), HttpStatus.CREATED, TaskResponseDTO.class);
        PatchDTO patchDTO = PatchDTO.builder()
                .assigned(4444L)
                .build();
        ErrorDetails errorDetails = helper.patchTask(created.getId(), patchDTO, LoginControllerTestHelper.getAccessToken(userToken), HttpStatus.NOT_FOUND, ErrorDetails.class);
        Assert.assertEquals("User does not exists by requested id: 4444", errorDetails.getMessage());

    }
}
