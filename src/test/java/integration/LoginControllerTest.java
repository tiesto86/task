package integration;

import com.behavox.dto.ErrorDetails;
import com.behavox.dto.auth.JwtAuthenticationResponse;
import com.behavox.dto.auth.LoginDTO;
import integration.helpers.LoginControllerTestHelper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;

public class LoginControllerTest extends TestsSetup {

    private LoginControllerTestHelper helper;

    @Before
    public void before() throws Exception {
        super.setup();
        this.helper = LoginControllerTestHelper.start(mvc);
    }

    @Test
    public void doLoginWithWrongCredentialsTest() throws Exception{
        LoginDTO wronguserName = LoginDTO.builder()
                .password("admin")
                .username("aa@bb.com")
                .build();
        ErrorDetails errorDetails = helper.doLogin(wronguserName, HttpStatus.UNAUTHORIZED, ErrorDetails.class);
        Assert.assertEquals("Bad credentials", errorDetails.getMessage());

        LoginDTO wrongPassword = LoginDTO.builder()
                .password("admin2")
                .username("admin@test.com")
                .build();
        errorDetails = helper.doLogin(wrongPassword, HttpStatus.UNAUTHORIZED, ErrorDetails.class);
        Assert.assertEquals("Bad credentials", errorDetails.getMessage());
    }

    @Test
    public void doLoginWithCorrectCredentialsTest() throws Exception{
        LoginDTO admin = LoginDTO.builder()
                .password("admin")
                .username("admin@test.com")
                .build();
        JwtAuthenticationResponse response = helper.doLogin(admin, HttpStatus.OK, JwtAuthenticationResponse.class);
        Assert.assertEquals("Bearer", response.getTokenType());
        Assert.assertNotNull(response.getAccessToken());

        LoginDTO user = LoginDTO.builder()
                .password("user")
                .username("user@test.com")
                .build();
        response = helper.doLogin(user, HttpStatus.OK, JwtAuthenticationResponse.class);
        Assert.assertEquals("Bearer", response.getTokenType());
        Assert.assertNotNull(response.getAccessToken());
    }
}
