package integration;

import com.behavox.dto.ErrorDetails;
import com.behavox.dto.auth.JwtAuthenticationResponse;
import com.behavox.dto.auth.LoginDTO;
import com.behavox.dto.tasks.PatchDTO;
import com.behavox.dto.tasks.TaskDTO;
import com.behavox.dto.tasks.TaskResponseDTO;
import com.behavox.dto.workflow.WorkflowDTO;
import com.behavox.entity.Workflow;
import com.behavox.entity.WorkflowSchema;
import com.behavox.enums.NotificationTypes;
import com.behavox.repository.WorkflowRepository;
import com.behavox.repository.WorkflowSchemaRepository;
import integration.helpers.LoginControllerTestHelper;
import integration.helpers.TasksControllerTestHelper;
import integration.helpers.WorkflowControllerTestHelper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import java.util.List;

public class WorkflowControllerTest extends TestsSetup {

    private WorkflowControllerTestHelper helper;

    private TasksControllerTestHelper taskHelper;

    private LoginControllerTestHelper authhelper;

    @Autowired
    private WorkflowSchemaRepository workflowSchemaRepository;

    @Autowired
    private WorkflowRepository workflowRepository;

    private JwtAuthenticationResponse adminToken;

    private JwtAuthenticationResponse userToken;

    @Before
    public void before() throws Exception {
        super.setup();
        this.helper = WorkflowControllerTestHelper.start(mvc);
        this.authhelper = LoginControllerTestHelper.start(mvc);
        this.taskHelper = TasksControllerTestHelper.start(mvc);

        LoginDTO admin = LoginDTO.builder()
                .password("admin")
                .username("admin@test.com")
                .build();
        adminToken = authhelper.doLogin(admin, HttpStatus.OK, JwtAuthenticationResponse.class);
        LoginDTO user = LoginDTO.builder()
                .password("user")
                .username("user@test.com")
                .build();
        userToken = authhelper.doLogin(user, HttpStatus.OK, JwtAuthenticationResponse.class);
        this.cleanUp();
    }

    private void cleanUp() {
        workflowRepository.deleteAll();
        workflowSchemaRepository.deleteAll();
    }

    @Test
    public void getAllWorkflowsTest() throws Exception {
        helper.createWorkflow(WorkflowControllerTestHelper.getWorkflowToCreate(1L), LoginControllerTestHelper.getAccessToken(adminToken), HttpStatus.CREATED, WorkflowDTO.class);
        List<WorkflowDTO> workflowDTOList = helper.getAllWorkflows(LoginControllerTestHelper.getAccessToken(adminToken), HttpStatus.OK, WorkflowDTO.class);
        Assert.assertEquals(1, workflowDTOList.size());
        WorkflowDTO workflowDTO = workflowDTOList.get(0);
        assertWorkflow(workflowDTO);
    }

    @Test
    public void getWorkflowByIdTest() throws Exception {
        WorkflowDTO createdWorkflowDTO = helper.createWorkflow(WorkflowControllerTestHelper.getWorkflowToCreate(1L), LoginControllerTestHelper.getAccessToken(adminToken), HttpStatus.CREATED, WorkflowDTO.class);
        WorkflowDTO workflowDTO = helper.getWorkflowById(createdWorkflowDTO.getId(), LoginControllerTestHelper.getAccessToken(adminToken), HttpStatus.OK, WorkflowDTO.class);
        assertWorkflow(workflowDTO);
    }

    @Test
    public void deleteWorkflowByIdTest() throws Exception {
        WorkflowDTO workflowDTO = helper.createWorkflow(WorkflowControllerTestHelper.getWorkflowToCreate(1L), LoginControllerTestHelper.getAccessToken(adminToken), HttpStatus.CREATED, WorkflowDTO.class);
        helper.deleteWorkflowById(workflowDTO.getId(), LoginControllerTestHelper.getAccessToken(adminToken), HttpStatus.NO_CONTENT);
        List<WorkflowDTO> workflowDTOList = helper.getAllWorkflows(LoginControllerTestHelper.getAccessToken(adminToken), HttpStatus.OK, WorkflowDTO.class);
        Assert.assertEquals(0, workflowDTOList.size());

        ErrorDetails errorDetails = helper.getWorkflowById(workflowDTO.getId(), LoginControllerTestHelper.getAccessToken(adminToken), HttpStatus.NOT_FOUND, ErrorDetails.class);
        Assert.assertEquals("Workflow has not been found by given id: " + workflowDTO.getId(), errorDetails.getMessage());

        //just to make sure
        List<WorkflowSchema> schemas = ((List<WorkflowSchema>)workflowSchemaRepository.findAll());
        List<Workflow> workflows = ((List<Workflow>)workflowRepository.findAll());
        Assert.assertEquals(0, schemas.size());
        Assert.assertEquals(0, workflows.size());
    }

    @Test
    public void createWorkflowForSameTaskTypeTest() throws Exception {
        helper.createWorkflow(WorkflowControllerTestHelper.getWorkflowToCreate(1L), LoginControllerTestHelper.getAccessToken(adminToken), HttpStatus.CREATED, WorkflowDTO.class);
        ErrorDetails errorDetails = helper.createWorkflow(WorkflowControllerTestHelper.getWorkflowToCreate(1L), LoginControllerTestHelper.getAccessToken(adminToken), HttpStatus.BAD_REQUEST,  ErrorDetails.class);
        Assert.assertEquals("Task type has been already assigned to another workflow. Task type id: 1", errorDetails.getMessage());
    }

    @Test
    public void updateWorkflowTest() throws Exception {
        WorkflowDTO workflowDTO = helper.createWorkflow(WorkflowControllerTestHelper.getWorkflowToCreate(1L), LoginControllerTestHelper.getAccessToken(adminToken), HttpStatus.CREATED, WorkflowDTO.class);
        WorkflowDTO updateworkflowDTO = WorkflowControllerTestHelper.getWorkflowToUpdate(workflowDTO.getId(), "test", workflowDTO.getTaskTypeIds());
        WorkflowDTO updatedworkflowDTO = helper.updateWorkflow(updateworkflowDTO, LoginControllerTestHelper.getAccessToken(adminToken), HttpStatus.OK, WorkflowDTO.class);

        WorkflowDTO getworkflowDTO = helper.getWorkflowById(updatedworkflowDTO.getId(), LoginControllerTestHelper.getAccessToken(adminToken), HttpStatus.OK, WorkflowDTO.class);

        Assert.assertEquals("test", getworkflowDTO.getDescription());
        Assert.assertEquals(1, getworkflowDTO.getTaskTypeIds().size());
        Assert.assertTrue(getworkflowDTO.getTaskTypeIds().contains(1L));
        Assert.assertEquals(1, getworkflowDTO.getWorkflowSteps().size());
        WorkflowDTO.WorkflowStepDTO step = getworkflowDTO.getWorkflowSteps().get(0);
        Assert.assertEquals(1, step.getStatusId().intValue());
        Assert.assertEquals(2, step.getAllowedActions().size());
    }

    @Test
    public void workflowIntegrationTest() throws Exception {
        TaskDTO taskDTO = TasksControllerTestHelper.getTaskToCreate();
        ErrorDetails error = taskHelper.createTask(taskDTO, LoginControllerTestHelper.getAccessToken(adminToken), HttpStatus.BAD_REQUEST, ErrorDetails.class);
        Assert.assertEquals("Task type doesn't associated with any workflow. Please contact bug tracking admin!", error.getMessage());

        WorkflowDTO workflowDTO = helper.createWorkflow(WorkflowControllerTestHelper.getWorkflowToCreate(1L), LoginControllerTestHelper.getAccessToken(adminToken), HttpStatus.CREATED, WorkflowDTO.class);
        TaskResponseDTO taskResponseDTO = taskHelper.createTask(taskDTO, LoginControllerTestHelper.getAccessToken(adminToken), HttpStatus.CREATED, TaskResponseDTO.class);

        //by existing workflow we could only change task into progress
        PatchDTO patchDTO = PatchDTO.builder()
                .status(3L)//resolved
                .build();
        ErrorDetails errorDetails = taskHelper.patchTask(taskResponseDTO.getId(), patchDTO, LoginControllerTestHelper.getAccessToken(userToken), HttpStatus.BAD_REQUEST, ErrorDetails.class);
        Assert.assertEquals("This action is not allowed! Next possible action: IN PROGESS", errorDetails.getMessage());

        patchDTO.setStatus(2L);//in progress - should be good
        taskHelper.patchTask(taskResponseDTO.getId(), patchDTO, LoginControllerTestHelper.getAccessToken(userToken), HttpStatus.OK, TaskResponseDTO.class);

        TaskResponseDTO newtaskResponseDTO = taskHelper.createTask(taskDTO, LoginControllerTestHelper.getAccessToken(adminToken), HttpStatus.CREATED, TaskResponseDTO.class);

        WorkflowDTO updateworkflowDTO = WorkflowControllerTestHelper.getWorkflowToUpdate(workflowDTO.getId(), "test", workflowDTO.getTaskTypeIds());
        helper.updateWorkflow(updateworkflowDTO, LoginControllerTestHelper.getAccessToken(adminToken), HttpStatus.OK, WorkflowDTO.class);

        patchDTO.setStatus(3L);//after update we should support flow open -> resolved
        taskHelper.patchTask(newtaskResponseDTO.getId(), patchDTO, LoginControllerTestHelper.getAccessToken(userToken), HttpStatus.OK, TaskResponseDTO.class);

    }

    private void assertWorkflow(WorkflowDTO workflowDTO) {
        Assert.assertEquals("test workflow", workflowDTO.getDescription());
        Assert.assertEquals(1, workflowDTO.getTaskTypeIds().size());
        Assert.assertTrue(workflowDTO.getTaskTypeIds().contains(1L));
        Assert.assertEquals(1, workflowDTO.getWorkflowSteps().size());
        WorkflowDTO.WorkflowStepDTO step = workflowDTO.getWorkflowSteps().get(0);
        Assert.assertEquals(1, step.getStatusId().intValue());
        Assert.assertEquals(1, step.getAllowedActions().size());
        WorkflowDTO.ActionStepDTO action = step.getAllowedActions().get(0);
        Assert.assertEquals(NotificationTypes.EMAIL, action.getAction());
        Assert.assertEquals(2, action.getAllowedStatusId().intValue());
    }
}
