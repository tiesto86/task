package integration.helpers;

import com.behavox.dto.tasks.PatchDTO;
import com.behavox.dto.tasks.TaskDTO;
import com.behavox.dto.tasks.TaskFilterDTO;
import com.behavox.dto.workflow.WorkflowDTO;
import com.behavox.enums.NotificationTypes;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import integration.TestsSetup;
import org.junit.Ignore;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;
import java.util.Set;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Ignore
public class WorkflowControllerTestHelper extends TestsSetup {

    private MockMvc mockMvc;

    public static WorkflowControllerTestHelper start(MockMvc mockMvc) {
        return new WorkflowControllerTestHelper(mockMvc);
    }

    public WorkflowControllerTestHelper(MockMvc mvc) {
        this.mockMvc = mvc;
    }

    public <T> T createWorkflow(WorkflowDTO workflowDTO, String token, HttpStatus httpStatus, Class<T> returnType) throws Exception{
        MvcResult result =  mockMvc.perform(post("/api/v1/workflow")
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(workflowDTO)))
                .andExpect(status().is(httpStatus.value())).andReturn();

        return  getObjectFromResponse(result, returnType);
    }

    public <T> T updateWorkflow(WorkflowDTO workflowDTO, String token, HttpStatus httpStatus, Class<T> returnType) throws Exception{
        MvcResult result =  mockMvc.perform(put("/api/v1/workflow")
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(workflowDTO)))
                .andExpect(status().is(httpStatus.value())).andReturn();

        return  getObjectFromResponse(result, returnType);
    }

    public <T> T getWorkflowById(Long id, String token, HttpStatus httpStatus, Class<T> returnType) throws Exception{
        MvcResult result =  mockMvc.perform(get("/api/v1/workflow/" + id)
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().is(httpStatus.value())).andReturn();

        return  getObjectFromResponse(result, returnType);
    }

    public <T> List<T> getAllWorkflows(String token, HttpStatus httpStatus, Class<T> returnType) throws Exception{
        MvcResult result =  mockMvc.perform(get("/api/v1/workflow")
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().is(httpStatus.value())).andReturn();

        return  getListFromResponse(result, returnType);
    }

    public void deleteWorkflowById(Long id, String token, HttpStatus httpStatus) throws Exception{
        mockMvc.perform(delete("/api/v1/workflow/" + id)
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().is(httpStatus.value()));
    }

    public static WorkflowDTO getWorkflowToCreate(Long taskId) {
        WorkflowDTO.WorkflowStepDTO progress = WorkflowDTO.WorkflowStepDTO.builder()
                .statusId(1L)//open
                .allowedActions(Lists.newArrayList(WorkflowDTO.ActionStepDTO.builder()
                        .allowedStatusId(2L)//in progress
                        .action(NotificationTypes.EMAIL)
                        .build()))
                .build();
        List<WorkflowDTO.WorkflowStepDTO> steps = Lists.newArrayList(progress);

        return WorkflowDTO.builder()
                .description("test workflow")
                .taskTypeIds(Sets.newHashSet(taskId))
                .workflowSteps(steps)
                .build();
    }

    public static WorkflowDTO getWorkflowToUpdate(Long id, String description, Set<Long> taskIds) {
        WorkflowDTO.WorkflowStepDTO progress = WorkflowDTO.WorkflowStepDTO.builder()
                .statusId(1L)//open
                .allowedActions(Lists.newArrayList(WorkflowDTO.ActionStepDTO.builder()
                                .allowedStatusId(2L)//in progress
                                .action(NotificationTypes.EMAIL)
                                .build(),
                        WorkflowDTO.ActionStepDTO.builder()
                                .allowedStatusId(3L)//resolved
                                .action(NotificationTypes.PUSH)
                                .build()))
                .build();

        List<WorkflowDTO.WorkflowStepDTO> steps = Lists.newArrayList(progress);
        return WorkflowDTO.builder()
                .id(id)
                .description(description)
                .taskTypeIds(taskIds)
                .workflowSteps(steps)
                .build();
    }
}
