package integration.helpers;

import com.behavox.dto.ErrorDetails;
import com.behavox.dto.auth.JwtAuthenticationResponse;
import com.behavox.dto.auth.LoginDTO;
import integration.TestsSetup;
import org.junit.Ignore;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Ignore
public class LoginControllerTestHelper extends TestsSetup {

    private MockMvc mockMvc;

    public static LoginControllerTestHelper start(MockMvc mockMvc) {
        return new LoginControllerTestHelper(mockMvc);
    }

    public LoginControllerTestHelper(MockMvc mvc) {
        this.mockMvc = mvc;
    }

    public <T> T doLogin(LoginDTO loginDTO, HttpStatus httpStatus, Class<T> returnType) throws Exception{
        MvcResult result =  mockMvc.perform(post("/login/auth")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(loginDTO)))
                .andExpect(status().is(httpStatus.value())).andReturn();

        return  getObjectFromResponse(result, returnType);
    }

    public static String getAccessToken(JwtAuthenticationResponse jwtAuthenticationResponse) {
        return jwtAuthenticationResponse.getTokenType() + " " + jwtAuthenticationResponse.getAccessToken();
    }
}
