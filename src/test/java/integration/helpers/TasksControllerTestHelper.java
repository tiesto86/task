package integration.helpers;

import com.behavox.dto.auth.LoginDTO;
import com.behavox.dto.tasks.PatchDTO;
import com.behavox.dto.tasks.TaskDTO;
import com.behavox.dto.tasks.TaskFilterDTO;
import integration.TestsSetup;
import org.junit.Ignore;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Ignore
public class TasksControllerTestHelper extends TestsSetup {

    private MockMvc mockMvc;

    public static TasksControllerTestHelper start(MockMvc mockMvc) {
        return new TasksControllerTestHelper(mockMvc);
    }

    public TasksControllerTestHelper(MockMvc mvc) {
        this.mockMvc = mvc;
    }

    public <T> T createTask(TaskDTO taskDTO, String token, HttpStatus httpStatus, Class<T> returnType) throws Exception{
        MvcResult result =  mockMvc.perform(post("/api/v1/tasks")
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(taskDTO)))
                .andExpect(status().is(httpStatus.value())).andReturn();

        return  getObjectFromResponse(result, returnType);
    }

    public <T> T updateTask(TaskDTO taskDTO, String token, HttpStatus httpStatus, Class<T> returnType) throws Exception{
        MvcResult result =  mockMvc.perform(put("/api/v1/tasks")
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(taskDTO)))
                .andExpect(status().is(httpStatus.value())).andReturn();

        return  getObjectFromResponse(result, returnType);
    }

    public <T> T getTaskById(Long id, String token, HttpStatus httpStatus, Class<T> returnType) throws Exception{
        MvcResult result =  mockMvc.perform(get("/api/v1/tasks/" + id)
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().is(httpStatus.value())).andReturn();

        return  getObjectFromResponse(result, returnType);
    }

    public <T> List<T> getMyTasks(TaskFilterDTO filter, String token, HttpStatus httpStatus, Class<T> returnType) throws Exception{
        MvcResult result =  mockMvc.perform(post("/api/v1/tasks/my")
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(filter)))
                .andExpect(status().is(httpStatus.value())).andReturn();

        return  getListFromResponse(result, returnType);
    }

    public <T> T deleteTaskById(Long id, String token, HttpStatus httpStatus, Class<T> returnType) throws Exception{
        MvcResult result =  mockMvc.perform(delete("/api/v1/tasks/" + id)
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().is(httpStatus.value())).andReturn();

        return  getObjectFromResponse(result, returnType);
    }

    public <T> T patchTask(Long id, PatchDTO patchDTO, String token, HttpStatus httpStatus, Class<T> returnType) throws Exception{
        MvcResult result =  mockMvc.perform(patch("/api/v1/tasks/" + id)
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(patchDTO)))
                .andExpect(status().is(httpStatus.value())).andReturn();

        return  getObjectFromResponse(result, returnType);
    }

    public void deleteTaskById(Long id, String token, HttpStatus httpStatus) throws Exception{
      mockMvc.perform(delete("/api/v1/tasks/" + id)
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().is(httpStatus.value()));
    }

    public static TaskDTO getTaskToCreate() {
        return TaskDTO.builder()
                .description("test descr")
                .name("test name")
                .taskTypeId(1L)
                .build();
    }

    public static TaskDTO getTaskToUpdate(Long id, String name, String description) {
        return TaskDTO.builder()
                .id(id)
                .description(description)
                .name(name)
                .build();
    }
}
