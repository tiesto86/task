package com.behavox.controller;

import com.behavox.dto.tasks.TaskResponseDTO;
import com.behavox.dto.workflow.WorkflowDTO;
import com.behavox.service.WorkflowService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api(value = "Workflow controller", description = "Workflow controller to operate workflow for tasks")
@RestController
@RequestMapping("/api/v1/workflow")
public class WorkflowController {

    @Autowired
    private WorkflowService workflowService;

    @ApiOperation(value = "Get workflow endpoint. Will return all workflows", response = WorkflowDTO.class, authorizations = {@Authorization("JWT")})
    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<List<WorkflowDTO>> getAllWorkflows() {
        List<WorkflowDTO> workflowDTOS = workflowService.getAllWorkflows();
        return new ResponseEntity<>(workflowDTOS, HttpStatus.OK);
    }

    @ApiOperation(value = "Get workflow by id endpoint. Will return workflow by provided id. If no workflow exists 404 will be returned", response = WorkflowDTO.class, authorizations = {@Authorization("JWT")})
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<WorkflowDTO> getWorkflowById(@PathVariable Long id) {
        WorkflowDTO workflow = workflowService.getWorkflowById(id);
        return new ResponseEntity<>(workflow, HttpStatus.OK);
    }

    @ApiOperation(value = "Create workflow endpoint. Should create new workflow", response = WorkflowDTO.class, authorizations = {@Authorization("JWT")})
    @RequestMapping(method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<WorkflowDTO> createWorkflow(@Valid @RequestBody WorkflowDTO workflowDTO) {
        WorkflowDTO workflow = workflowService.createWorkflow(workflowDTO);
        return new ResponseEntity<>(workflow, HttpStatus.CREATED);
    }

    @ApiOperation(value = "Update workflow endpoint. Should update existing workflow", response = WorkflowDTO.class, authorizations = {@Authorization("JWT")})
    @RequestMapping(method = RequestMethod.PUT)
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<WorkflowDTO> updateWorkflow(@Valid @RequestBody WorkflowDTO workflowDTO) {
        WorkflowDTO workflow = workflowService.updateWorkflow(workflowDTO);
        return new ResponseEntity<>(workflow, HttpStatus.OK);
    }

    @ApiOperation(value = "Delete workflow by id endpoint. Existing workflow should be phisically removed from DB. If no workflow exists 404 will be returned", authorizations = {@Authorization("JWT")})
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<WorkflowDTO> deleteWorkflow(@PathVariable Long id) {
        workflowService.deleteWorkflow(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
