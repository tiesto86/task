package com.behavox.controller;

import com.behavox.dto.tasks.TaskTypeDTO;
import com.behavox.service.TaskTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(value = "Task type controller", description = "Task types controller to get allowed statuses to UI")
@RestController
@RequestMapping("/api/v1/tasks/types")
public class TaskTypeController {

    @Autowired
    private TaskTypeService taskTypeService;

    @ApiOperation(value = "Get all task types to pupulate dropdown on UI", response = TaskTypeDTO.class, authorizations = {@Authorization("JWT")})
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<TaskTypeDTO>> getAllTaskTypes() {
        List<TaskTypeDTO> taskTypeDTOS = taskTypeService.getTaskTypes();
        return new ResponseEntity<>(taskTypeDTOS, HttpStatus.OK);
    }
}
