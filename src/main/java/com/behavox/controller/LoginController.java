package com.behavox.controller;

import com.behavox.dto.auth.JwtAuthenticationResponse;
import com.behavox.dto.auth.LoginDTO;
import com.behavox.service.LoginService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Api(value = "Login controller", description = "Login controller to authentificate users")
@RestController
@RequestMapping("/login")
public class LoginController {

    @Autowired
    private LoginService loginService;

    @ApiOperation(value = "API request to generate token", response = JwtAuthenticationResponse.class)
    @RequestMapping(value = "/auth", method = RequestMethod.POST)
    public ResponseEntity<JwtAuthenticationResponse> authenticateUser(@Valid @RequestBody LoginDTO loginRequest) {
        JwtAuthenticationResponse jwt = loginService.doLogin(loginRequest);
        return new ResponseEntity<>(jwt, HttpStatus.OK);
    }
}
