package com.behavox.controller;

import com.behavox.annotation.CurrentUser;
import com.behavox.dto.tasks.PatchDTO;
import com.behavox.dto.tasks.TaskDTO;
import com.behavox.dto.tasks.TaskFilterDTO;
import com.behavox.dto.tasks.TaskResponseDTO;
import com.behavox.security.model.UserPrincipal;
import com.behavox.service.TaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api(value = "Task controller", description = "Task controller to operate tasks actions")
@RestController
@RequestMapping("/api/v1/tasks")
public class TaskController {

    @Autowired
    private TaskService taskService;

    @ApiOperation(value = "Create task endpoint. User should have ADMIN role", response = TaskResponseDTO.class, authorizations = {@Authorization("JWT")})
    @RequestMapping(method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<TaskResponseDTO> createTask(@Valid @RequestBody TaskDTO taskDTO) {
        TaskResponseDTO task = taskService.createTask(taskDTO);
        return new ResponseEntity<>(task, HttpStatus.CREATED);
    }

    @ApiOperation(value = "Update task endpoint. User should have ADMIN role", response = TaskResponseDTO.class, authorizations = {@Authorization("JWT")})
    @RequestMapping(method = RequestMethod.PUT)
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<TaskResponseDTO> updateTask(@Valid @RequestBody TaskDTO taskDTO) {
        TaskResponseDTO task = taskService.updateTask(taskDTO);
        return new ResponseEntity<>(task, HttpStatus.OK);
    }

    @ApiOperation(value = "End point to retrieve task by existing ID", response = TaskResponseDTO.class, authorizations = {@Authorization("JWT")})
    @RequestMapping(value ="/{id}", method = RequestMethod.GET)
    public ResponseEntity<TaskResponseDTO> getTask(@PathVariable Long id) {
        TaskResponseDTO task = taskService.getTaskById(id);
        return new ResponseEntity<>(task, HttpStatus.OK);
    }

    @ApiOperation(value = "Delete task endpoint (actually it will deactivate task)", response = TaskResponseDTO.class, authorizations = {@Authorization("JWT")})
    @RequestMapping(value ="/{id}", method = RequestMethod.DELETE)
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<Void> deleteTask(@PathVariable Long id) {
        taskService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ApiOperation(value = "POST request to get tasks by filter in body", response = TaskResponseDTO.class, authorizations = {@Authorization("JWT")})
    @RequestMapping(value ="/my", method = RequestMethod.POST)
    public ResponseEntity<List<TaskResponseDTO>> getMyTasks(@CurrentUser UserPrincipal userPrincipal, @RequestBody TaskFilterDTO taskFilterDTO) {
        List<TaskResponseDTO> tasks = taskService.getMyTasks(userPrincipal, taskFilterDTO);
        return new ResponseEntity<>(tasks, HttpStatus.OK);
    }

    @ApiOperation(value = "Partially update task (assign task, change status)", response = TaskResponseDTO.class, authorizations = {@Authorization("JWT")})
    @RequestMapping(value ="/{id}", method = RequestMethod.PATCH)
    public ResponseEntity<TaskResponseDTO> patchTask(@PathVariable Long id, @RequestBody PatchDTO patchTask) {
        TaskResponseDTO taskResponseDTO = taskService.patchTask(id, patchTask);
        return new ResponseEntity<>(taskResponseDTO, HttpStatus.OK);
    }
}
