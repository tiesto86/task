package com.behavox.controller;

import com.behavox.dto.StatusDTO;
import com.behavox.service.StatusService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(value = "Task status controller", description = "Task status controller to get allowed statuses to UI")
@RestController
@RequestMapping("/api/v1/tasks/statuses")
public class TaskStatusController {

    @Autowired
    private StatusService statusService;

    @ApiOperation(value = "Get all task statuses to pupulate dropdown on UI", response = StatusDTO.class, authorizations = {@Authorization("JWT")})
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<StatusDTO>> getAllTaskStatuses() {
        List<StatusDTO> statusDTOS = statusService.getStatuses();
        return new ResponseEntity<>(statusDTOS, HttpStatus.OK);
    }
}
