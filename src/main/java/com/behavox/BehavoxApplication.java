package com.behavox;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
@EnableCaching
@EntityScan(basePackages = { "com.behavox.entity" })
@EnableJpaRepositories(basePackages = { "com.behavox.repository" })
@EnableJpaAuditing
@SpringBootApplication
public class BehavoxApplication extends SpringBootServletInitializer {

  public static void main(String[] args) {
    SpringApplication.run(BehavoxApplication.class, args);
  }

}
