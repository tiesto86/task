package com.behavox.repository;

import com.behavox.entity.Status;
import com.behavox.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface StatusRepository extends CrudRepository<Status, Long> {

    List<Status> findAllByName(String name);

}
