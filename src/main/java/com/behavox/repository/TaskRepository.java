package com.behavox.repository;

import com.behavox.entity.Task;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface TaskRepository extends CrudRepository<Task, Long> {

    Optional<Task> findByActiveTrueAndId(@Param("id") Long id);

    List<Task> findAllByAssignedUserAndActiveIsTrue(@Param("id") Long id);

}
