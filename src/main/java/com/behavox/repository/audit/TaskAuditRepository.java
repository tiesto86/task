package com.behavox.repository.audit;

import com.behavox.entity.audit.TaskAudit;
import org.springframework.data.repository.CrudRepository;

public interface TaskAuditRepository extends CrudRepository<TaskAudit, Integer> {
}
