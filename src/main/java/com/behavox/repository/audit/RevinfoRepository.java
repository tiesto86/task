package com.behavox.repository.audit;

import com.behavox.entity.audit.Revinfo;
import org.springframework.data.repository.CrudRepository;

public interface RevinfoRepository extends CrudRepository<Revinfo, Integer> {
}
