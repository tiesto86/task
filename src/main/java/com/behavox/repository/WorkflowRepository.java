package com.behavox.repository;

import com.behavox.entity.Workflow;
import org.springframework.data.repository.CrudRepository;

public interface WorkflowRepository extends CrudRepository<Workflow, Long> {
}
