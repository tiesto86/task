package com.behavox.repository;

import com.behavox.entity.TaskType;
import com.behavox.entity.Workflow;
import org.springframework.data.repository.CrudRepository;

public interface TaskTypeRepository extends CrudRepository<TaskType, Long> {
}
