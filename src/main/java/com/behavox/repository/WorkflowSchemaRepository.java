package com.behavox.repository;

import com.behavox.entity.WorkflowSchema;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface WorkflowSchemaRepository extends CrudRepository<WorkflowSchema, Long> {

    @Modifying
    @Query(value = "delete from workflow_schema where workflow_id = :id", nativeQuery = true)
    void deleteByWorkflowId(@Param("id") Long id);

}
