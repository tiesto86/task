package com.behavox.exceptionhandler;

import com.behavox.dto.ErrorDetails;
import com.behavox.exception.BehavoxException;
import com.behavox.exception.BehavoxNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;

@ControllerAdvice
@Slf4j
public class BehavoxExceptionHandler {

    /**
     * Catch all unhandled exception
     * If it's business exception BehavoxException we return 400 - bad request. In another cases 500 - Internal server error
     * @param ex unhandled exception
     * @param request incoming request
     * @return return response with injected ErrorDTO object
     */
    @ExceptionHandler(Exception.class)
    public final ResponseEntity<ErrorDetails> handleAllExceptions(Exception ex, WebRequest request) {
        log.error(ex.getMessage());
        ex.printStackTrace();

        if (ex instanceof AccessDeniedException) {
            return new ResponseEntity<>(getErrorDTO(ex, request), HttpStatus.FORBIDDEN);
        } else if (ex instanceof BehavoxNotFoundException) {
            return new ResponseEntity<>(getErrorDTO(ex, request), HttpStatus.NOT_FOUND);
        } else if (ex instanceof BadCredentialsException) {
            return new ResponseEntity<>(getErrorDTO(ex, request), HttpStatus.UNAUTHORIZED);
        } else if (ex instanceof BehavoxException || ex instanceof MissingServletRequestParameterException) {
            return new ResponseEntity<>(getErrorDTO(ex, request), HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity<>(getErrorDTO(ex, request), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private ErrorDetails getErrorDTO(Exception ex,  WebRequest request) {
        return ErrorDetails.builder()
                .timestamp(new Date())
                .details(request.getDescription(false))
                .message(ex.getMessage())
                .build();
    }
}
