package com.behavox.service;

import com.behavox.dto.tasks.PatchDTO;
import com.behavox.dto.tasks.TaskDTO;
import com.behavox.dto.tasks.TaskFilterDTO;
import com.behavox.dto.tasks.TaskResponseDTO;
import com.behavox.entity.Status;
import com.behavox.entity.Task;
import com.behavox.enums.NotificationTypes;
import com.behavox.exception.BehavoxNotFoundException;
import com.behavox.repository.TaskRepository;
import com.behavox.security.model.UserPrincipal;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Service
public class TaskService {

	@Autowired
	private TaskRepository taskRepository;

	@Autowired
	private StatusService statusUtils;

	@Autowired
	private NotificationService notificationService;

	@Autowired
	private UserService userService;

	@Autowired
	private WorkflowService workflowService;

	@Autowired
	private TaskTypeService taskTypeUtils;

	/**
	 * Create new task.
	 * @param task
	 * @return
	 */
	public TaskResponseDTO createTask(TaskDTO task) {
		Task modified;
		Status status = statusUtils.getDefaultStatus();

		NotificationTypes notificationTypes = taskTypeUtils.getNotificationTypeByTaskTypeAndStatusId(task.getTaskTypeId(), status.getId());
		Task newTask = Task.builder()
				.assignedUser(task.getAssigned())
				.name(task.getName())
				.description(task.getDescription())
				.status(status)
				.active(Boolean.TRUE)
				.taskType(taskTypeUtils.enrichTaskTypeEntity(task.getTaskTypeId()))
				.build();
		modified = taskRepository.save(newTask);

		CompletableFuture.runAsync(() -> notificationService.sendNotification(modified.getStatus().getId(), notificationTypes));
		return taskResponseDTO(modified);
	}

	/**
	 * Update task.
	 *
	 * @param task - task DTO to update
	 * @return
	 */
	public TaskResponseDTO updateTask(TaskDTO task) {
		Task current, modified;

		current = findTaskById(task.getId());
		current.setAssignedUser(task.getAssigned());
		current.setName(task.getName());
		current.setDescription(task.getDescription());
		modified = taskRepository.save(current);

		NotificationTypes notificationTypes = taskTypeUtils.getNotificationTypeByTaskTypeAndStatusId(current.getTaskType().getId(), current.getStatus().getId());

		CompletableFuture.runAsync(() -> notificationService.sendNotification(modified.getStatus().getId(), notificationTypes));
		return taskResponseDTO(modified);
	}

	/**
	 * Find active task by id.
	 * If no task has been found exception will be thrown
	 * @param id
	 * @return
	 */
	public TaskResponseDTO getTaskById(Long id) {
		Task task = findTaskById(id);
		return taskResponseDTO(task);
	}

	/**
	 * Delete task. Actually it's deactivating task
	 * @param id
	 */
	public void deleteById(Long id) {
		Task task = findTaskById(id);
		task.setActive(Boolean.FALSE);
		taskRepository.save(task);
	}

	/**
	 * Will return only my task by ffilter
	 * @param user
	 * @param taskFilterDTO
	 * @return
	 */
	public List<TaskResponseDTO> getMyTasks(UserPrincipal user, TaskFilterDTO taskFilterDTO) {
		List<Task> tasks = CollectionUtils.isEmpty(taskFilterDTO.getStatuses())
				? taskRepository.findAllByAssignedUserAndActiveIsTrue(user.getId())
				: taskRepository.findAllByAssignedUserAndActiveIsTrue(user.getId())
				.stream()
				.filter(task -> taskFilterDTO.getStatuses().contains(task.getStatus().getId()))
				.collect(Collectors.toList());

		return tasks.stream().map(task -> taskResponseDTO(task))
				.collect(Collectors.toList());
	}

	/**
	 * Will partially update task.
	 * Assigning user or task status change
	 * @param id
	 * @param patchDTO
	 * @return
	 */
	public TaskResponseDTO patchTask(Long id, PatchDTO patchDTO) {
		Task task = findTaskById(id);
		if(patchDTO.getAssigned() != null) {
			Boolean userExists = userService.userExists(patchDTO.getAssigned());
			if (!userExists) {
				throw new BehavoxNotFoundException("User does not exists by requested id: " + patchDTO.getAssigned());
			}
			task.setAssignedUser(patchDTO.getAssigned());
		}
		if(patchDTO.getStatus() != null) {
			workflowService.validateWorkFlow(task.getTaskType().getId(), task.getStatus().getId(), patchDTO.getStatus());
			task.setStatus(statusUtils.enrichStatusById(patchDTO.getStatus()));
		}
		Task updated = taskRepository.save(task);

		NotificationTypes notificationTypes = taskTypeUtils.getNotificationTypeByTaskTypeAndStatusId(updated.getTaskType().getId(), updated.getStatus().getId());
		CompletableFuture.runAsync(() -> notificationService.sendNotification(updated.getStatus().getId(), notificationTypes));

		return taskResponseDTO(updated);
	}

	/**
	 * get activate task by id
	 * @param id
	 * @return
	 */
	private Task findTaskById(Long id) {
		return taskRepository.findByActiveTrueAndId(id)
				.orElseThrow(() -> new BehavoxNotFoundException("Task not found by id: " + id));
	}

	/**
	 * converter from task entity to task DTO
	 * @param task
	 * @return
	 */
	private TaskResponseDTO taskResponseDTO(Task task) {
		return TaskResponseDTO.builder()
				.id(task.getId())
				.assigned(task.getAssignedUser())
				.name(task.getName())
				.description(task.getDescription())
				.status(task.getStatus().getName())
				.build();
	}
}
