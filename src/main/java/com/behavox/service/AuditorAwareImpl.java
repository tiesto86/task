package com.behavox.service;

import com.behavox.security.model.UserPrincipal;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

public class AuditorAwareImpl implements AuditorAware<Long> {

	@Override
	public Optional<Long> getCurrentAuditor() {
		return Optional.ofNullable(((UserPrincipal)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId());
	}
}
