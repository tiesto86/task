package com.behavox.service;

import com.behavox.cache.CacheEvictUtil;
import com.behavox.dto.StatusDTO;
import com.behavox.entity.Status;
import com.behavox.exception.BehavoxException;
import com.behavox.exception.BehavoxNotFoundException;
import com.behavox.repository.StatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class StatusService {

	public final static String OPENED = "OPEN";

	@Autowired
	private StatusRepository statusRepository;

	/**
	 * Return statuses. We will cache it. To evict cache please use CacheEvictUtil
	 * For now there is no case where we need to evict cache
	 * @return
	 */
	@Cacheable(value = CacheEvictUtil.STATUS_CACHE)
	public List<StatusDTO> getStatuses() {
		return ((List<Status>)statusRepository.findAll()).stream()
				.map(status -> StatusDTO.builder()
						.description(status.getDescription())
						.id(status.getId())
						.name(status.getName())
						.build())
				.collect(Collectors.toList());
	}

	/**
	 * Will return default status for create task - OPEN
	 * @return
	 */
	public Status getDefaultStatus() {
		return statusRepository.findAllByName(OPENED).stream()
				.findFirst()
				.orElseThrow(() -> new BehavoxException("no opened"));
	}

	/**
	 * Enrich status entity by id
	 * @param id
	 * @return
	 */
	public Status enrichStatusById(Long id) {
		return statusRepository.findById(id)
				.orElseThrow(() -> new BehavoxNotFoundException("Not found status by ID: " + id));
	}

	/**
	 * Will provide next actions text
	 * @param ids
	 * @return
	 */
	public String nextActions(Set<Long> ids) {
		return ((List<Status>)statusRepository.findAllById(ids)).stream()
				.map(status -> status.getName())
				.collect(Collectors.joining(","));
	}
}
