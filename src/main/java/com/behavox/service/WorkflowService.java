package com.behavox.service;

import com.behavox.dto.workflow.WorkflowDTO;
import com.behavox.entity.TaskType;
import com.behavox.entity.Workflow;
import com.behavox.entity.WorkflowSchema;
import com.behavox.exception.BehavoxException;
import com.behavox.exception.BehavoxNotFoundException;
import com.behavox.repository.WorkflowRepository;
import com.behavox.repository.WorkflowSchemaRepository;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class WorkflowService {

    @Autowired
    private WorkflowRepository workflowRepository;

    @Autowired
    private WorkflowSchemaRepository workflowSchemaRepository;

    @Autowired
    private TaskTypeService taskTypeService;

    @Autowired
    private StatusService statusService;

    /**
     * will return workflow by ID
     * @param id
     * @return
     */
    public WorkflowDTO getWorkflowById(Long id) {
        Workflow workflow = findWorkflowEntityById(id);
        return fromEntity(workflow);
    }

    /**
     * Will return all workflows
     * @return
     */
    public List<WorkflowDTO> getAllWorkflows() {
        List<Workflow> workflowList = ((List<Workflow>)workflowRepository.findAll());
        return workflowList.stream()
                .map(this::fromEntity)
                .collect(Collectors.toList());
    }

    /**
     * Create new workflow
     * @param workflowDTO
     * @return
     */
    public WorkflowDTO createWorkflow(WorkflowDTO workflowDTO) {
        Workflow workflow = Workflow.builder()
                .description(workflowDTO.getDescription())
                .taskTypes(workflowDTO.getTaskTypeIds().stream()
                        .map(id -> {
                            TaskType tasktype = taskTypeService.enrichTaskTypeEntity(id);
                            if (tasktype.getWorkflow() != null) {
                                throw new BehavoxException("Task type has been already assigned to another workflow. Task type id: " + id);
                            }
                            return tasktype;
                        })
                        .collect(Collectors.toSet()))
                .build();

        List<WorkflowSchema> schemas = toWorkflowSchema(workflowDTO, workflow);
        workflow.setSchema(schemas);
        Workflow created = workflowRepository.save(workflow);

        return fromEntity(created);
    }

    /**
     * Update existing workflow by  ID
     * @param workflowDTO
     * @return
     */
    @Transactional
    public WorkflowDTO updateWorkflow(WorkflowDTO workflowDTO) {
        Workflow workflow = findWorkflowEntityById(workflowDTO.getId());
        workflowSchemaRepository.deleteByWorkflowId(workflowDTO.getId());
        workflow.setSchema(Lists.newArrayList());

        workflow.setDescription(workflowDTO.getDescription());
        workflow.setTaskTypes(workflowDTO.getTaskTypeIds().stream()
                .map(id -> {
                    TaskType tasktype = taskTypeService.enrichTaskTypeEntity(id);
                    if (tasktype.getWorkflow() != null && !tasktype.getWorkflow().getId().equals(workflow.getId())) {
                        throw new BehavoxException("Task type has been already assigned to another workflow. Task type id: " + id);
                    }
                    return tasktype;
                })
                .collect(Collectors.toSet()));

        List<WorkflowSchema> schemas = toWorkflowSchema(workflowDTO, workflow);
        workflow.setSchema(schemas);
        Workflow updated = workflowRepository.save(workflow);

        return fromEntity(updated);
    }

    /**
     * delete existing workflow by ID
     * @param id
     */
    public void deleteWorkflow(Long id) {
        Workflow workflow = findWorkflowEntityById(id);
        workflowRepository.delete(workflow);
    }

    /**
     * Validate task wworkflow if next request action is OK
     * @param taskTypeId
     * @param currentStatusId
     * @param targetStatusId
     */
    public void validateWorkFlow(Long taskTypeId, Long currentStatusId, Long targetStatusId) {
        statusService.enrichStatusById(targetStatusId);
        TaskType taskType = taskTypeService.enrichTaskTypeEntity(taskTypeId);
        Set<Long> schemaListIds = taskType.getWorkflow().getSchema().stream()
                .filter(schema -> schema.getStatus().getId().equals(currentStatusId))
                .map(schema -> schema.getAllowedStatus().getId())
                .collect(Collectors.toSet());
        if (!schemaListIds.contains(targetStatusId)) {
            String actions = statusService.nextActions(schemaListIds);
            throw new BehavoxException("This action is not allowed! Next possible action: " + actions);
        }
    }

    /**
     * Will return entity by reqi=uested ID
     * @param id
     * @return
     */
    private Workflow findWorkflowEntityById(Long id){
        return  workflowRepository.findById(id)
                .orElseThrow(() -> new BehavoxNotFoundException("Workflow has not been found by given id: " + id));
    }

    /**
     * Converter
     * @param workflowDTO
     * @param workflow
     * @return
     */
    private List<WorkflowSchema> toWorkflowSchema(WorkflowDTO workflowDTO, Workflow workflow) {
        List<WorkflowSchema> schemas = Lists.newArrayList();
        workflowDTO.getWorkflowSteps().forEach(step -> {
            step.getAllowedActions().forEach(action -> {
                WorkflowSchema workflowSchema = WorkflowSchema.builder()
                        .workflow(workflow)
                        .action(action.getAction())
                        .status(statusService.enrichStatusById(step.getStatusId()))
                        .allowedStatus(statusService.enrichStatusById(action.getAllowedStatusId()))
                        .build();
                schemas.add(workflowSchema);
            });
        });

        return schemas;
    }

    private WorkflowDTO fromEntity(Workflow entity) {
        List<WorkflowDTO.WorkflowStepDTO> workflowSteps = Lists.newArrayList();
        entity.getSchema().stream()
                .collect(Collectors.groupingBy(WorkflowSchema::getStatus))
                .entrySet().forEach(entry -> {
            WorkflowDTO.WorkflowStepDTO stepDTO = WorkflowDTO.WorkflowStepDTO.builder()
                    .allowedActions(entry.getValue().stream().map(allowed -> WorkflowDTO.ActionStepDTO.builder()
                            .action(allowed.getAction())
                            .allowedStatusId(allowed.getAllowedStatus().getId())
                            .build())
                            .collect(Collectors.toList()))
                    .statusId(entry.getKey().getId())
                    .build();
            workflowSteps.add(stepDTO);
        });
        return WorkflowDTO.builder()
                .id(entity.getId())
                .description(entity.getDescription())
                .taskTypeIds(entity.getTaskTypes().stream()
                        .map(task -> task.getId())
                        .collect(Collectors.toSet()))
                .workflowSteps(workflowSteps)
                .build();
    }
}
