package com.behavox.service;

import com.behavox.enums.NotificationTypes;
import com.behavox.exception.BehavoxException;
import com.behavox.service.notifications.interfaces.NotificationInterface;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class NotificationService {

    private Map<NotificationTypes, NotificationInterface> notificationMap;

    @Autowired
    public void setRouting(Collection<NotificationInterface> notifications) {
        if (CollectionUtils.isEmpty(notifications)) {
            this.notificationMap = Collections.emptyMap();
        } else {
            Map<NotificationTypes, NotificationInterface> result = new HashMap<>(notifications.size());
            notifications.forEach(notificator -> {
                NotificationInterface previousIntegration = result.putIfAbsent(notificator.getType(), notificator);
                if (previousIntegration != null) {
                    String message = String.format(
                            "Non unique '%s' notification service found. Classes: %s, %s",
                            notificator.getType(), previousIntegration.getClass().getName(), notificator.getClass().getName());
                    throw new BehavoxException(message);
                }
            });
            this.notificationMap = result;
        }
    }

    /**
     * will send notification by appropriate processor
     * @param id
     * @param notificationTypes
     */
    public void sendNotification(Long id, NotificationTypes notificationTypes) {
        if(notificationMap.containsKey(notificationTypes)) {
            notificationMap.get(notificationTypes).doPostTransitionAction();
        } else {
            log.debug("No notification processor has been found for action id: {}", id);
        }
    }
}
