package com.behavox.service;

import com.behavox.dto.auth.JwtAuthenticationResponse;
import com.behavox.dto.auth.LoginDTO;
import com.behavox.repository.RoleRepository;
import com.behavox.repository.UserRepository;
import com.behavox.security.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;


@Service
public class LoginService {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtTokenProvider tokenProvider;

    @Autowired
    private RoleRepository roleRepository;

    /**
     * Authentificate user. If username and password ok the system will generate and return signed JWT token
     * @param loginDTO
     * @return
     */
    public JwtAuthenticationResponse doLogin(LoginDTO loginDTO) {
        Authentication authentication;
        try {
            authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            loginDTO.getUsername(),
                            loginDTO.getPassword()
                    )
            );
        } catch (BadCredentialsException bce) {
            throw bce;
        } catch (Exception ex) {
            throw ex;
        }

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = tokenProvider.generateToken(authentication);

        return JwtAuthenticationResponse.builder()
                .accessToken(jwt)
                .build();

    }

}
