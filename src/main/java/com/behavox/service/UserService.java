package com.behavox.service;

import com.behavox.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public Boolean userExists(Long userId) {
        return userRepository.existsById(userId);
    }
}
