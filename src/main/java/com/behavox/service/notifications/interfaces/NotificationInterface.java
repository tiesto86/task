package com.behavox.service.notifications.interfaces;

import com.behavox.enums.NotificationTypes;

public interface NotificationInterface {

    public NotificationTypes getType();

    public void doPostTransitionAction();

}
