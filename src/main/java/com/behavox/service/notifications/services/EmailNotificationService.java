package com.behavox.service.notifications.services;

import com.behavox.enums.NotificationTypes;
import com.behavox.service.notifications.interfaces.NotificationInterface;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class EmailNotificationService implements NotificationInterface {

    @Override
    public NotificationTypes getType() {
        return NotificationTypes.EMAIL;
    }

    /**
     * Service to send email. Will be executed asynchronusly so it should not effect on flow
     * Even exception throwing should not effect on currect request
     */
    @Override
    public void doPostTransitionAction() {
        log.info("Sending email");
        throw new NullPointerException();
    }
}
