package com.behavox.service.notifications.services;

import com.behavox.enums.NotificationTypes;
import com.behavox.service.notifications.interfaces.NotificationInterface;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class PushNotificationService implements NotificationInterface {

    @Override
    public NotificationTypes getType() {
        return NotificationTypes.PUSH;
    }

    /**
     * Service to send push notification. Will be executed asynchronusly so it should not effect on flow
     * Even exception throwing should not effect on currect request
     */
    @Override
    public void doPostTransitionAction() {
        log.info("Sending push notification");
        throw new NullPointerException();
    }
}
