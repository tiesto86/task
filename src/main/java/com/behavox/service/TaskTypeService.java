package com.behavox.service;

import com.behavox.cache.CacheEvictUtil;
import com.behavox.dto.tasks.TaskTypeDTO;
import com.behavox.entity.TaskType;
import com.behavox.enums.NotificationTypes;
import com.behavox.exception.BehavoxException;
import com.behavox.repository.TaskTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class TaskTypeService {

    @Autowired
    private TaskTypeRepository taskTypeRepository;

    /**
     * Will enrich taskType entity by id
     * @param id
     * @return
     */
    public TaskType enrichTaskTypeEntity(Long id) {
        return taskTypeRepository.findById(id)
                .orElseThrow(() -> new BehavoxException("Couldn't find task type by id: " + id));
    }

    /**
     * Will return notification type
     * @param taskTypeId
     * @param statusId
     * @return
     */
    public NotificationTypes getNotificationTypeByTaskTypeAndStatusId(Long taskTypeId, Long statusId) {
        TaskType taskType = enrichTaskTypeEntity(taskTypeId);
        if (taskType.getWorkflow() == null) {
            throw new BehavoxException("Task type doesn't associated with any workflow. Please contact bug tracking admin!");
        }
        return taskType.getWorkflow().getSchema().stream()
                .filter(schema -> schema.getStatus().getId().equals(statusId))
                .findFirst()
                .map(schema -> schema.getAction())
                .orElse(null);
    }

    /**
     * will return task types on UI
     * @return
     */
    @Cacheable(value = CacheEvictUtil.TASK_TYPE_CACHE)
    public List<TaskTypeDTO> getTaskTypes() {
        return ((List<TaskType>)taskTypeRepository.findAll()).stream()
                .map(status -> TaskTypeDTO.builder()
                        .description(status.getDescription())
                        .id(status.getId())
                        .type(status.getType())
                        .build())
                .collect(Collectors.toList());
    }
}
