package com.behavox.exception;

public class BehavoxNotFoundException extends RuntimeException {

    public BehavoxNotFoundException() {
        super();
    }

    public BehavoxNotFoundException(Throwable e) {
        super(e);
    }

    public BehavoxNotFoundException(String message, Throwable e) {
        super(message, e);
    }

    public BehavoxNotFoundException(String message) {
        super(message);
    }

}
