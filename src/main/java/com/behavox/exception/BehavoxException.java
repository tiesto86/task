package com.behavox.exception;

public class BehavoxException extends RuntimeException {

    public BehavoxException() {
        super();
    }

    public BehavoxException(Throwable e) {
        super(e);
    }

    public BehavoxException(String message, Throwable e) {
        super(message, e);
    }

    public BehavoxException(String message) {
        super(message);
    }

}
