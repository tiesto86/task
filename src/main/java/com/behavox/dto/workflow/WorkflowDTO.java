package com.behavox.dto.workflow;

import com.behavox.enums.NotificationTypes;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.List;
import java.util.Set;

@ApiModel(description = "Request to create/update workflow")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WorkflowDTO {

	@ApiModelProperty(notes = "Workflow unique ID to")
	private Long id;
	@ApiModelProperty(notes = "Workflow description")
	private String description;
	@ApiModelProperty(notes = "Task type IDs to link existing workflow")
	private Set<Long> taskTypeIds;

	private List<WorkflowStepDTO> workflowSteps;

	@ApiModel(description = "Transition DTO to define link between status and allowed action ID's")
	@AllArgsConstructor
	@NoArgsConstructor
	@Getter
	@Setter
	@Builder
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public final static class WorkflowStepDTO {
		@ApiModelProperty(notes = "Status ID to build transition allowed action ID")
		private Long statusId;
		private List<ActionStepDTO> allowedActions;
	}

	@ApiModel(description = "Patch request to generate JWT token")
	@AllArgsConstructor
	@NoArgsConstructor
	@Getter
	@Setter
	@Builder
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public final static class ActionStepDTO {
		@ApiModelProperty(notes = "Task transition allowed action ID")
		private Long allowedStatusId;
		@ApiModelProperty(notes = "Task action description. By this we will specify if we want to send some kind of notification or not")
		private NotificationTypes action;
	}

}
