package com.behavox.dto.auth;

import lombok.*;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class LoginDTO {

    @NotBlank
    private String username;

    @NotBlank
    private String password;

}
