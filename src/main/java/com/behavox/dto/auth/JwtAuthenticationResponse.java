package com.behavox.dto.auth;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class JwtAuthenticationResponse {

    private String accessToken;
    private final String tokenType = "Bearer";

}
