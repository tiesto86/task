package com.behavox.dto.tasks;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

@ApiModel(description = "Response with task type details")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class TaskTypeDTO {
    @ApiModelProperty(notes = "Task type unique ID")
    private Long id;
    @ApiModelProperty(notes = "Task type name in system")
    private String type;
    @ApiModelProperty(notes = "Task type description")
    private String description;
}
