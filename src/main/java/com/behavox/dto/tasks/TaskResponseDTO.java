package com.behavox.dto.tasks;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

@ApiModel(description = "Response DTO with Task model")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class TaskResponseDTO {

	@ApiModelProperty(notes = "Task unique ID in system")
	private Long id;
	@ApiModelProperty(notes = "Task name")
	private String name;
	@ApiModelProperty(notes = "Task general description")
	private String description;
	@ApiModelProperty(notes = "Task assignee")
	private Long assigned;
	@ApiModelProperty(notes = "Task current status")
	private String status;

}
