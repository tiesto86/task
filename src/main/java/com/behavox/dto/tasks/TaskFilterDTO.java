package com.behavox.dto.tasks;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.List;

@ApiModel(description = "Request DTO to get tasks")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class TaskFilterDTO {

	@ApiModelProperty(notes = "List of statuses ID's to filter tasks")
	private List<Long> statuses;

}
