package com.behavox.dto.tasks;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

@ApiModel(description = "Patch request to generate JWT token")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class PatchDTO {

	@ApiModelProperty(notes = "Task status ID to update")
	private Long status;
	@ApiModelProperty(notes = "Task assigned user ID")
	private Long assigned;

}
