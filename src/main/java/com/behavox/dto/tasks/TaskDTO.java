package com.behavox.dto.tasks;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.NotNull;

@ApiModel(description = "Request DTO to create/update task")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class TaskDTO {

	@ApiModelProperty(notes = "Task unique ID in system")
	private Long id;
	@ApiModelProperty(notes = "Task name")
	@NotNull
	private String name;
	@ApiModelProperty(notes = "Task general description")
	@NotNull
	private String description;
	@ApiModelProperty(notes = "Asignee user's ID")
	private Long assigned;
	@ApiModelProperty(notes = "Task type ID")
	private Long taskTypeId;

}
