package com.behavox.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@ApiModel(description = "Response with task status details")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class StatusDTO {
	@ApiModelProperty(notes = "Status unique ID")
	private Long id;
	@ApiModelProperty(notes = "Status name in the system")
	private String name;
	@ApiModelProperty(notes = "Status business description")
	private String description;
}
