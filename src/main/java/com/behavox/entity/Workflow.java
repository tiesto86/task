package com.behavox.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "workflow")
public class Workflow implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "workflow_generator")
    @SequenceGenerator(name = "workflow_generator", sequenceName = "workflow_seq", allocationSize = 1)
    private Long id;

    private String description;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "task_type_workflow",
            joinColumns = @JoinColumn(name = "workflow_id"),
            inverseJoinColumns = @JoinColumn(name = "task_type_id"))
    private Set<TaskType> taskTypes = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "workflow")
    private List<WorkflowSchema> schema;

}
