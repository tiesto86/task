package com.behavox.entity;

import com.behavox.entity.composite.WorkflowSchemaCompositeKey;
import com.behavox.enums.NotificationTypes;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@IdClass(WorkflowSchemaCompositeKey.class)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "workflow_schema")
public class WorkflowSchema implements Serializable {

    @Id
    @ManyToOne
    @MapsId
    @JoinColumn(name = "workflow_id", referencedColumnName = "id")
    private Workflow workflow;

    @Id
    @OneToOne
    @JoinColumn(name = "status_id", referencedColumnName = "id", updatable = false, insertable = false)
    private Status status;

    @Id
    @OneToOne
    @JoinColumn(name = "allowed_status_id", referencedColumnName = "id", updatable = false, insertable = false)
    private Status allowedStatus;

    @Enumerated(EnumType.STRING)
    private NotificationTypes action;

}
