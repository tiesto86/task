package com.behavox.entity.composite;

import com.behavox.entity.Status;
import com.behavox.entity.Workflow;
import lombok.*;

import java.io.Serializable;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class WorkflowSchemaCompositeKey implements Serializable {

    private Workflow workflow;
    private Status allowedStatus;
    private Status status;
    
}
