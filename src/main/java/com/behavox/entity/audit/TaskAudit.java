package com.behavox.entity.audit;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "task_aud")
public class TaskAudit {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "task_aud_generator")
    @SequenceGenerator(name = "task_aud_generator", sequenceName = "task_aud_seq", allocationSize = 1)
    @Column(name = "REV")
    private Integer rev;

    @Column(name = "REVTYPE")
    private Short timestamp;

    @Column(name = "created_by")
    private Long createdBy;

    @Column(name = "updated_by")
    private Long updatedBy;

    private Long assignee;

    private Boolean active;

}
