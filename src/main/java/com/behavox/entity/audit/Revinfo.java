package com.behavox.entity.audit;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "revinfo")
public class Revinfo {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "revinfo_generator")
    @SequenceGenerator(name = "revinfo_generator", sequenceName = "revinfo_seq", allocationSize = 1)
    @Column(name = "rev")
    private Integer id;

    @Column(name = "revtstmp")
    private Long timestamp;

}
