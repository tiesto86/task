package com.behavox.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "task_type")
public class TaskType implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "task_type_generator")
    @SequenceGenerator(name = "task_type_generator", sequenceName = "task_type_seq", allocationSize = 1)
    private Long id;

    private String description;

    private String type;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinTable(name = "task_type_workflow",
            inverseJoinColumns = @JoinColumn(name = "workflow_id"),
            joinColumns = @JoinColumn(name = "task_type_id"))
    private Workflow workflow;

}
