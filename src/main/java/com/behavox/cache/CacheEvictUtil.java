package com.behavox.cache;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Component;

@Component
public class CacheEvictUtil {

	public static final String STATUS_CACHE = "STATUS";

	public static final String TASK_TYPE_CACHE = "TASK_TYPE";

	@CacheEvict(value = STATUS_CACHE, beforeInvocation = true)
	public void evictStatusCache() {

	}

	@CacheEvict(value = TASK_TYPE_CACHE, beforeInvocation = true)
	public void evictTaskTypeCache() {

	}
}
