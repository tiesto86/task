package com.behavox.cache;

import com.google.common.collect.Lists;
import org.springframework.boot.autoconfigure.cache.CacheManagerCustomizer;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CacheCustomizer implements CacheManagerCustomizer<ConcurrentMapCacheManager> {

	@Override
	public void customize(ConcurrentMapCacheManager cacheManager) {
       List<String> cache = Lists.newArrayList();

       cache.add(CacheEvictUtil.STATUS_CACHE);
       cache.add(CacheEvictUtil.TASK_TYPE_CACHE);

       cacheManager.setCacheNames(cache);
	}
}
