--
-- users
--
CREATE SEQUENCE IF NOT EXISTS user_id_seq START 1;
CREATE SEQUENCE IF NOT EXISTS user_role_seq START 1;

CREATE TABLE users (
    id bigint NOT NULL DEFAULT nextval('user_id_seq'::regclass) primary key,
    email VARCHAR(128) UNIQUE,
    password VARCHAR(255),
    first_name VARCHAR(255),
    last_name VARCHAR(255),
    failed_login_attempts integer DEFAULT 0,
    active boolean DEFAULT TRUE,
    createddate timestamp DEFAULT localtimestamp(0),
    updateddate timestamp DEFAULT localtimestamp(0)
);

CREATE TABLE roles (
  id bigint NOT NULL DEFAULT nextval('user_role_seq'::regclass) primary key,
  name varchar(10) NOT NULL UNIQUE
);


CREATE TABLE user_roles (
  user_id bigint NOT NULL,
  role_id bigint NOT NULL,
  PRIMARY KEY (user_id, role_id)
)