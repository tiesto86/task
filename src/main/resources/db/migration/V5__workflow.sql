--
-- workflow
--
CREATE SEQUENCE IF NOT EXISTS task_type_seq START 1;
CREATE SEQUENCE IF NOT EXISTS workflow_seq START 1;

CREATE TABLE IF NOT EXISTS workflow
(
    id bigint NOT NULL DEFAULT nextval('workflow_seq'::regclass) primary key,
    description text
);

CREATE TABLE IF NOT EXISTS task_type
(
    id bigint NOT NULL DEFAULT nextval('task_type_seq'::regclass) primary key,
    description text,
    type VARCHAR(40) NOT NULL
);

CREATE TABLE IF NOT EXISTS workflow_schema
(
    workflow_id bigint,
    status_id bigint,
    allowed_status_id bigint,
    action text,
    primary key(workflow_id, status_id, allowed_status_id),
    foreign key (workflow_id) references workflow(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS task_type_workflow (
  task_type_id bigint NOT NULL,
  workflow_id bigint NOT NULL,
  PRIMARY KEY (task_type_id, workflow_id)
);

insert into task_type(type, description) values('TASK', 'Resolve task or technical debt');
insert into task_type(type, description) values('BUG', 'Fix problem or defect in the system');
insert into task_type(type, description) values('STORY', 'Implement new feature');
insert into task_type(type, description) values('IMPROVEMENT', 'Implement technical improvement');

ALTER TABLE status DROP COLUMN allowed_actions;
ALTER TABLE task ADD COLUMN task_type_id bigint;

--we will create workflow and schema for workflow according to the main task
--this workflov we will map for task_type=TASK


--create workflov
insert into workflow(description) values('General workflow');

--map workflov to task (here we will support multiple workflow) One workflow will be linked to appropriate task_type(BUG, TASK, STORY etc)
insert into task_type_workflow(task_type_id, workflow_id) values(1, 1);

--we will create workflow scheama for TASK issue
insert into workflow_schema(workflow_id, status_id, allowed_status_id, action) values (1, 1, 3, 'EMAIL'); --from open to resolved send email
insert into workflow_schema(workflow_id, status_id, allowed_status_id, action) values (1, 1, 2, 'PUSH');  --from open to in progress send push notification
insert into workflow_schema(workflow_id, status_id, allowed_status_id) values (1, 1, 5); --from open to closed do not send anything
insert into workflow_schema(workflow_id, status_id, allowed_status_id) values (1, 2, 1);
insert into workflow_schema(workflow_id, status_id, allowed_status_id) values (1, 2, 3);
insert into workflow_schema(workflow_id, status_id, allowed_status_id) values (1, 2, 5);
insert into workflow_schema(workflow_id, status_id, allowed_status_id) values (1, 3, 4);
insert into workflow_schema(workflow_id, status_id, allowed_status_id) values (1, 5, 4);
insert into workflow_schema(workflow_id, status_id, allowed_status_id) values (1, 4, 2);
insert into workflow_schema(workflow_id, status_id, allowed_status_id) values (1, 4, 3);
insert into workflow_schema(workflow_id, status_id, allowed_status_id) values (1, 4, 5);