--
-- audit
--
CREATE SEQUENCE IF NOT EXISTS revinfo_seq START 1;
CREATE SEQUENCE IF NOT EXISTS task_aud_seq START 1;

CREATE SEQUENCE IF NOT EXISTS hibernate_sequence  INCREMENT 1  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE IF NOT EXISTS revinfo
(
    rev integer NOT NULL DEFAULT nextval('revinfo_seq'::regclass) primary key,
    revtstmp bigint
);

CREATE TABLE IF NOT EXISTS task_aud
(
    REV integer NOT NULL DEFAULT nextval('task_aud_seq'::regclass) primary key,
    REVTYPE smallint,
    id bigint NOT NULL,
    assignee bigint,
    created_by bigint,
    updated_by bigint,
    active boolean
);
