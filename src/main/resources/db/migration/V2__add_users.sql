--
-- users
--
INSERT INTO roles (name) values ('ADMIN');
INSERT INTO roles (name) values ('USER');

INSERT INTO users(email, password, first_name, last_name, active) values ('admin@test.com', '$2a$10$Ho0ligAOou8fdiWYfue4m.y6q5t4FTxb9s.lzOh96nab43HGlJVSW', 'Admin First Name', 'Admin Last Name', true);
INSERT INTO users(email, password, first_name, last_name, active) values ('user@test.com', '$2a$10$M6gaT6X2B8SsJIYm2fMYAeDUle/I8AggygrnI.GAXfj79g1plRviC', 'User First Name', 'User Last Name', true);
INSERT INTO user_roles (user_id, role_id) values (1, 1);
INSERT INTO user_roles (user_id, role_id) values (2, 2);