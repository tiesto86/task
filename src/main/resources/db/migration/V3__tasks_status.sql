--
-- users
--
CREATE SEQUENCE IF NOT EXISTS task_id_seq START 1;
CREATE SEQUENCE IF NOT EXISTS status_id_seq START 1;

CREATE TABLE task (
    id bigint NOT NULL DEFAULT nextval('task_id_seq'::regclass) primary key,
    name text,
    description text,
    assignee bigint,
    created_by bigint,
    updated_by bigint,
    active boolean DEFAULT TRUE,
    status_id bigint,
    created_date timestamp DEFAULT localtimestamp(0),
    updated_date timestamp DEFAULT localtimestamp(0)
);

CREATE TABLE status (
  id bigint NOT NULL DEFAULT nextval('status_id_seq'::regclass) primary key,
  name text,
  description text,
  allowed_actions jsonb
);

insert into status(name, description, allowed_actions) values('OPEN', 'Task opened', '[3, 5, 2]');
insert into status(name, description, allowed_actions) values('IN PROGESS', 'Start Progress', '[1, 3, 5]');
insert into status(name, description, allowed_actions) values('RESOLVED', 'Resolved issue', '[4]');
insert into status(name, description, allowed_actions) values('REOPENED', 'Reopened issue', '[3, 2, 5]');
insert into status(name, description, allowed_actions) values('CLOSED', 'Close issue', '[4]');
